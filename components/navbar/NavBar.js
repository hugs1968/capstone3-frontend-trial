//import "./Navbar.scss";
import {useContext} from 'react'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import Jumbotron from 'react-bootstrap/Jumbotron'
import Link from 'next/link'
import avatar from "../../public/avatar.svg";

import UserContext from '../../UserContext'


export default function NavBar({ sidebarOpen, openSidebar }){

	const {user} = useContext(UserContext)

	//console.log(user)

	 return (
    <nav className="navbar">
      <div className="nav_icon" onClick={() => openSidebar()}>
        <i className="fa fa-bars" aria-hidden="true"></i>
      </div>
      <div className="navbar__left">        
        <a className="active_link" id="nav-budget-tracker" href="#">
          Budget Tracker
        </a>
      </div>
      <div className="navbar__right">
	{
        user.email
        ?
		<>
		<a href="#!">
			<img width="30" src={avatar} alt="avatar" />
		</a>
		</>
		:
		<>
		<Link href="/register">
		<a className="active_link nav-button" role="button">Register</a>
		</Link>	

		<Link href="/login">
		<a className="active_link nav-button" role="button">Log in</a>
		</Link>	
							
		<a href="#!">
			<img width="30" src={avatar} alt="avatar" />
		</a>
		</>
	}


        
      </div>
    </nav>
  );
};







/*

			<Navbar bg="dark" variant="dark" expand="lg">
						<Link href="/">
							<a className="navbar-brand">Budget Friendly</a>
						</Link>
				<Navbar.Toggle aria-controls="basic-navbar-nav" />
				<Navbar.Collapse id="basic-navbar-nav">
						<Nav className="mr-auto">
						{
							user.email
							?
							<>
							<Link href="/categories">
							<a className="nav-link" role="button">Categories</a>
							</Link>

							<Link href="/records">
							<a className="nav-link" role="button">Records</a>
							</Link>

							<Link href="/charts/monthly-income">
							<a className="nav-link" role="button">Monthly Income</a>
							</Link>

							<Link href="/charts/monthly-expense">
							<a className="nav-link" role="button">Monthly Expense</a>
							</Link>

							<Link href="/charts/balance-trend">
							<a className="nav-link" role="button">Trend</a>
							</Link>

							<Link href="/charts/category-breakdown">
							<a className="nav-link" role="button">Breakdown</a>
							</Link>

							<Link href="/logout">
							<a className="nav-link" role="button">Log out</a>
							</Link>	
							</>						
							:
							<>
							<Link href="/register">
							<a className="nav-link" role="button">Register</a>
							</Link>	

							<Link href="/login">
							<a className="nav-link" role="button">Log in</a>
							</Link>	
							</>	

						}
							
							

						</Nav>
				</Navbar.Collapse>
			</Navbar>



*/