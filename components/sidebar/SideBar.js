//import "./Sidebar.module.sass";
import logo from "../../public/rsz_kokoro.png";
import {useContext, useEffect, Fragment} from 'react'
import ViewHeadlineIcon from '@material-ui/icons/ViewHeadline';
import HomeIcon from '@material-ui/icons/Home';
import FormatListBulletedIcon from '@material-ui/icons/FormatListBulleted';
import LibraryBooksIcon from '@material-ui/icons/LibraryBooks';
import InsertChartIcon from '@material-ui/icons/InsertChart';
import TrendingUpIcon from '@material-ui/icons/TrendingUp';
import PieChartIcon from '@material-ui/icons/PieChart';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import InfoIcon from '@material-ui/icons/Info';
import UserContext from '../../UserContext'


export default function SideBar({ sidebarOpen, closeSidebar }) {

  const {user} = useContext(UserContext)

  //console.log(user)

  return (

    <div className={sidebarOpen ? "sidebar_responsive" : ""} id="sidebar">
      <div className="sidebar__title">
        <div className="sidebar__img">
          <img src={logo} alt="logo" />
          <h1>Budget Friendly</h1>
        </div>
        <i
          onClick={() => closeSidebar()}
          className="fa fa-times"
          id="sidebarIcon"
          aria-hidden="true"
        ></i>
      </div>
      {
        user.email
        ?
        <Fragment>
          <div className="sidebar__menu">
            <div className="sidebar__link active_menu_link">
              <i className="fa-home"><HomeIcon /></i>
              <a href="/home">Dashboard</a>
            </div>
            <h2>Tools</h2>
            <div className="sidebar__link">
              <i className="fa fa-user-bullet" aria-hidden="true"><FormatListBulletedIcon /></i>
              <a href="/categories">Categories</a>
            </div>
            <div className="sidebar__link">
              <i className="fa fa-records-o"><LibraryBooksIcon /></i>
              <a href="/records">Records</a>
            </div>
            <div className="sidebar__link">
              <i className="fa fa-chart"><InsertChartIcon /></i>
              <a href="/charts/monthly-income">Monthly Income</a>
            </div>
            <div className="sidebar__link">
              <i className="fa fa-chart"><InsertChartIcon /></i>
              <a href="/charts/monthly-expense">Monthly Expense</a>
            </div>
            <div className="sidebar__link">
              <i className="fa fa-line-chart"><TrendingUpIcon /></i>
              <a href="/charts/balance-trend">Trend</a>
            </div>        
            <div className="sidebar__link">
              <i className="fa fa-breakdown"><PieChartIcon /></i>
              <a href="/charts/category-breakdown">Breakdown</a>
            </div>        
            <h2>Settings</h2>
              <div className="sidebar__link">
                <i className="fa fa-money"><AccountCircleIcon /></i>
                <a href="#">User Profile</a>
              </div>
              <div className="sidebar__link">
                <i className="fa fa-briefcase"><InfoIcon /></i>
                <a href="/about">About Us</a>
              </div>
              <div className="sidebar__logout">
                <i className="fa fa-power-off"></i>
                <a href="/logout">Log out</a>
              </div>          
          </div>
        </Fragment>
        :
        <Fragment>
          <div className="sidebar__menu">
            <div className="sidebar__link active_menu_link">
              <i className="fa-home"><HomeIcon /></i>
              <a href="/home">Dashboard</a>
            </div>
            <h2>Settings</h2>
            <div className="sidebar__link">
              <i className="fa fa-briefcase"><InfoIcon /></i>
              <a href="/about">About Us</a>
            </div>  
          </div>
        </Fragment>

      }
      
    </div>
    
  );
};