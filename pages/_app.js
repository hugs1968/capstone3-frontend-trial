import '../styles/globals.css'
import {useState,useEffect} from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react'
import {Container} from 'react-bootstrap'
import NavBar from '../components/navbar/NavBar'
import SideBar from '../components/sidebar/SideBar'
import {UserProvider} from '../UserContext'
import {Row,Col} from 'react-bootstrap'


function MyApp({ Component, pageProps }) {

/*new*/
  const [sidebarOpen, setsidebarOpen] = useState(false);
  const openSidebar = () => {
    setsidebarOpen(true);
  }
  const closeSidebar = () => {
    setsidebarOpen(false);
  }
/*end*/

  const [user,setUser] = useState({

    id: null,
    email: null   

  })


useEffect(()=>{

      fetch('https://boiling-oasis-07638.herokuapp.com/api/users/details',{

        headers: {

          Authorization: `Bearer ${localStorage.getItem('token')}`

        }

      })
      .then(res => res.json())
      .then(data => {

          if(data._id){

              setUser({

              id: data._id,
              email: data.email              

          })


          } else {

              setUser({

              id: null,
              email: null
          })


        }

        

      })


  }, []) 


  const unsetUser = () => {
    localStorage.clear()
    setUser({
      id: null,
      email: null,
    })
  }



  return (

    <UserProvider value={{user,setUser,unsetUser}}>
      <div className="container-fluid px-0">       
        <Row>
          <Col md={3}>
          <SideBar sidebarOpen={sidebarOpen} closeSidebar={closeSidebar} />
          </Col>   
          <Col md={9}>
          <NavBar sidebarOpen={sidebarOpen} openSidebar={openSidebar} />  
        <Component {...pageProps} />
        </Col>       
      </Row> 
      </div>
    </UserProvider>

    )
}

export default MyApp
