import {useState,useEffect} from 'react'
import AppHelper from '../../app_helper.js'
import BarChart from './../../components/BarChart'

export default function MonthlyExpense(){

	const [monthlyExpense, setMonthlyExpense] = useState([])

	//this will fetch all the data at initial render. and store the data in a state
	useEffect(()=>{
		let tempArr = []
		const datum = {
			headers: {
				Authorization: `Bearer ${AppHelper.getAccessToken()}`
			}
		}

		fetch(`${AppHelper.API_URL}/users/records`, datum)
		.then(res => res.json())
		.then(data => {
			//console.log(data)			
			data.map(record => {
				if(record.categoryType === "Expense"){
					//const date = moment(record.dateMade).format('MMMM')
					tempArr.push(record)
				}
			})
			//console.log(tempArr)
			setMonthlyExpense(tempArr)
		})
	},[])

	//console.log(monthlyExpense)

	return(
		<>
			<h2 className="text-center">Monthly Expense</h2>
			<BarChart data={monthlyExpense}/>
		</>
		)
}