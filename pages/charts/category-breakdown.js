import {Fragment,useState,useEffect} from 'react'
import AppHelper from '../../app_helper.js'
//import PieBreakdown from './../../components/PieChart'
import {Row,Col} from 'react-bootstrap'
import {Container} from 'react-bootstrap'
import moment from 'moment'
import Form from 'react-bootstrap/Form'
import {Pie} from 'react-chartjs-2'


export default function CategoryBreakdown(){

	const [dateFrom,setDateFrom] = useState('')
	const [dateTo,setDateTo] = useState('')

	//categoryData is an array state that we pass as props to piechart
	const [categoryData, setCategoryData] = useState([])

	//filteredData is an array state filtered according to the dates set 
	const [filteredData, setFilteredData] = useState([])
	const [names, setNames] = useState([])
	const [dataPerCategories, setDataPerCategories] = useState([])

	//const [breakdown, setBreakdown] = useState([])


	//this will set the dateFrom and dateTo 
	useEffect(()=> {
		let today = new Date();
		setDateTo(moment(today).format('YYYY-MM-DD'))
		setDateFrom(moment(today).subtract(31, 'days').format('YYYY-MM-DD'))
	},[])

	//this will fetch all the data at initial render. and store the data in a state
	useEffect(()=>{
		
		const datum = {
			headers: {
				Authorization: `Bearer ${AppHelper.getAccessToken()}`
			}
		}

		fetch(`${AppHelper.API_URL}/users/records`, datum)
		.then(res => res.json())
		.then(data => {
			//console.log(data)			
			setCategoryData(data)
		})

		
		
	},[])

	//this will filter the data once the all the records are retrieved, and every time the dates are changed and store them in an array state
	useEffect(()=>{
		let tempArr = []

		categoryData.forEach(record => {
			//console.log(record)
			let recordDate = moment(record.dateMade).format('YYYY-MM-DD')
			if(recordDate >= dateFrom && recordDate <= dateTo){
				//console.log(record)
				tempArr.push(record)
			}				
		})
		//console.log(tempArr)
		setFilteredData(tempArr)

	},[categoryData, dateFrom, dateTo])

	//console.log(categoryData)
	//console.log(filteredData)

	//this will get all the names of categories made by user everytime filteredData is changed
	useEffect(()=>{
		//console.log(data)

		if(filteredData.length > 0){
			//console.log(data)
			let tempCategories = [] 
			filteredData.forEach(element => {
				//console.log(element)
				if(!tempCategories.find(category => category === element.categoryName)){
					tempCategories.push(element.categoryName)
				}
			})
			setNames(tempCategories)
		}

	},[filteredData])

	//console.log(names)
	
	//this will add all the records that fall within the same categories.
	useEffect(()=>{

		setDataPerCategories(names.map(name => {

			let total = 0

			filteredData.forEach(element => {
				if(element.categoryName === name){
					total = total + element.amount
				}
			})
			//console.log(total)
			return total
		}))
	},[names])

	
	return(
			<div className="mt-5 pt-4 mb-5">
			<Container>
			<Fragment>
			<h2 className="text-center">Category Breakdown</h2>
		
			<Form>
				<Row>
					<Col>				
						<Form.Group>
							<Form.Label>From</Form.Label>
							<Form.Control type="date" 
							className="form-control"
							value={dateFrom}
							onChange={e => setDateFrom(e.target.value)}
							placeholder="Start Date" />
						</Form.Group>
					</Col>
					<Col>
						<Form.Group>
							<Form.Label>To</Form.Label>
							<Form.Control type="date" 
							className="form-control" 
							value={dateTo} 
							onChange={e => setDateTo(e.target.value)}
							placeholder="End Date" />
						</Form.Group>
					</Col>				
				</Row>
			</Form>			
			<hr />
			<Pie data = {{

				labels: names,
				datasets:[{

					data: dataPerCategories,//array of numbers
					backgroundColor: ["green","lightblue","red"]
				
				}]

			}}/>

			</Fragment>
			</Container>
			</div>
		)
}