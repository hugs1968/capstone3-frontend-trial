import {useContext,useEffect,useState,Fragment} from 'react'
import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/Button'
import {Row, Col} from 'react-bootstrap'
import Image from 'next/image'
import icePic from '../../images/profile-picture-ice.png'
import tonyPic from '../../images/profile-picture-anthony-notcropped.png'


export default function About(){
	return(
		<Fragment>
		<h2 className="mt-3">About Us:</h2>
		<Row className="justify-content-md-center" style={{ height: '36rem' }}>
			<Col md={3}>
				<img src={icePic} alt="Ice" />
				<h2 className="text-center">Ice Benitez</h2>
				<p>Specializes in Express/Nodejs, React, and Data Visualization.</p>
			</Col>
			<Col md={4}>
				<Card className="mr-5">
				    <Card.Img variant="top" src={tonyPic} style={{ width: '100%' }}/>
				    <Card.Body>
				      <Card.Title>Anthony Castro</Card.Title>
				      <Card.Text>
				        Specialties are  HTML5, CSS3, Semantic Web, Responsive Layouts.
				      </Card.Text>
				    </Card.Body>
				  </Card>
			</Col>
		</Row>
		<p className="text-center">This API can help users to track their budget with monthly charts and graphs</p>
		</Fragment>
		)
}